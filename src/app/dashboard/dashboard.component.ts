import {Component, OnInit} from '@angular/core';
import {ApiService} from "../service/api.service";
import {Note} from "../model/note";
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialogComponent} from "./confirm-dialog/confirm-dialog.component";
import {trigger, state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('fadeAnimation', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({opacity: 1})),

      // fade in when created.
      transition(':enter', [
        style({opacity: 0}),
        animate(200)
      ]),

      // fade out when destroyed.
      transition(':leave',
        animate(400, style({opacity: 0})))
    ])
  ]
})
export class DashboardComponent implements OnInit {

  public notesData: Note[] = [];
  public note: Note = new Note();
  public noteForm: FormGroup;
  public isProgressBar: boolean = false;
  public isApiConnectionError: boolean = false;


  constructor(private api: ApiService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {

    /**
     * Gets all notes
     */
    this.getAllNotes('id', 'desc');
  }

  /**
   * Gets all notes from API endpoint.
   * If notes are exists and received and creates FormArray,
   * otherwise set isApiConnectionError for true.
   *
   * @param sortBy the value by which to sort the list.
   * @param orderBy the value by which to order the list.
   */
  public getAllNotes(sortBy: string, orderBy: string): void {

    this.isProgressBar = true;

    this.api.getNotes(sortBy, orderBy).subscribe(res => {
      this.notesData = res;
      if (this.notesData) {
        this.noteForm = this.formBuilder.group({
          'notes': this.formBuilder.array(this.notesData.map(note => this.createFormGroup(note))),
        });
      }
      this.isProgressBar = false;
    }, error => {
      this.isProgressBar = false;
      if (error.status !== 404) {
        this.isApiConnectionError = true;
      }
    })
  }

  /**
   * Creates a new note.
   * Adds new FormGroup to FormArray
   */
  public createNote(): void {

    this.api.createNote(this.note).subscribe(res => {
      this.snackBar.open('Post-it note has been created :)',
        '',
        {
          duration: 2000,
          panelClass: ['success-snackbar']
        });
      this.notesData.unshift(res);
      this.formData.insert(0, (this.createFormGroup(res)));

    }, error => {
      this.snackBar.open(
        'An error occurred during creating post-it note :(',
        '',
        {
          duration: 2000,
          panelClass: ['danger-snackbar']
        });
    })
  }

  /**
   * Updates the note.
   *
   * @param note object of `Note`.
   *
   * @param index number
   */
  public updateNote(note: Note, index: number): void {

    note.body = this.noteForm.value.notes[index].body;
    note.updatedAt = new Date();

    this.api.updateNote(note).subscribe(res => {
    }, error => {
      this.snackBar.open(
        'An error occurred during update post-it note :(',
        '',
        {
          duration: 2000,
          panelClass: ['danger-snackbar']
        });
    })
  }

  /**
   * Shows confirm delete dialog. If confirm dialog returns true
   * then deletes note, otherwise skip.
   *
   * @param note object of `Note`.
   *
   * @param index number
   */
  public onDeleteNote(note: Note, index: number): void {

    let dialogRef = this.dialog.open(ConfirmDialogComponent);

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
        this.api.deleteNote(note.id).subscribe(res => {

          this.snackBar.open(
            'Post-it note has been deleted :)',
            '',
            {
              duration: 2000,
              panelClass: ['success-snackbar']
            });
          this.notesData = this.notesData.filter(({id}) => id !== note.id);
          this.formData.removeAt(index);


        }, error => {
          this.snackBar.open(
            'An error occurred during delete post-it note :(',
            '',
            {
              duration: 2000,
              panelClass: ['danger-snackbar']
            });
        })
      }
    });

  }

  /**
   * Retrieves a child control given the control's name
   * and returns as `FormArray`
   *
   * @returns FormArray
   */
  get formData(): FormArray {

    return <FormArray>this.noteForm.get('notes');
  }

  /**
   * Creates `ForumGroup` with collection of child controls.
   * Helper function.
   *
   * @param note object of `Note`. Required to pass `body` value.
   *
   * @returns FormGroup with collection of child controls.
   */
  private createFormGroup(note: Note): FormGroup {

    return this.formBuilder.group({
      body: this.formBuilder.control({value: note.body, disabled: false})
    });
  }

}
