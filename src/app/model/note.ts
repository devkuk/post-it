export class Note {

  id: number;
  body: string;
  updatedAt: Date;
  createdAt: Date = new Date();

}
