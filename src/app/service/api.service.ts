import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {map, timeout} from "rxjs/operators";
import {Note} from "../model/note";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  /** Single request timeout value  */
  readonly requestTimout: number = 5000;


  constructor(private http: HttpClient) {
  }

  /**
   * Gets notes from API endpoint.
   *
   * @param sortBy the value by which to sort the list.
   * @param orderBy the value by which to order the list.
   *
   * @returns An `Observable` of the response for the request
   */
  public getNotes(sortBy: string, orderBy: string): Observable<any> {
    return this.http.get<any>(`${environment.apiBasePath}/notes?_sort=${sortBy}&_order=${orderBy}`)
      .pipe(timeout(this.requestTimout), map((res: any) => {
        return res;
      }))
  }

  /**
   * Posts the note.
   *
   * @param note object of `Note`.
   *
   * @returns An `Observable` of the response for the request
   */
  public createNote(note: Note): Observable<any> {
    return this.http.post<any>(`${environment.apiBasePath}/notes`, note)
      .pipe(timeout(this.requestTimout), map((res: any) => {
        return res;
      }))
  }

  /**
   * Updates the note.
   *
   * @param note object of `Note`.
   *
   * @returns An `Observable` of the response for the request
   */
  public updateNote(note: Note): Observable<any> {
    return this.http.put<any>(`${environment.apiBasePath}/notes/${encodeURIComponent(String(note.id))}`, note)
      .pipe(timeout(this.requestTimout), map((res: any) => {
        return res;
      }))
  }

  /**
   * Deletes the note.
   *
   * @param id of note.
   *
   * @returns An `Observable` of the response for the request
   */
  public deleteNote(id: number): Observable<any> {

    return this.http.delete<any>(`${environment.apiBasePath}/notes/${encodeURIComponent(String(id))}`)
      .pipe(timeout(this.requestTimout), map((res: any) => {
        return res;
      }))
  }

}
